import {Routes, RouterModule} from '@angular/router';
import {DashboardComponent} from './components/dashboard/dashboard';
import {HeroDetailComponent} from './components/hero-detail/component';
import {HeroesComponent} from './components/heroes/component';
import {NgModule} from '@angular/core';
import {GamesComponent} from "./components/games/component";
import {PlacesComponent} from "./components/places/component";
import {PlayersComponent} from "./components/players/players";
import {TeamsComponent} from "./components/teams/component";
import {StandingsComponent} from "./components/standings/standings";
import {RefereesComponent} from "./components/referees/referees";
import {PlayerComponent} from "./components/player_unused/player";

const routes: Routes = [
    {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
    {
        path: 'dashboard', component: DashboardComponent,
        children: [
            {path: 'test', component: StandingsComponent}
        ]
    },
    {path: 'heroes/:id', component: HeroDetailComponent},
    {path: 'heroes', component: HeroesComponent},

    {path: 'places', component: PlacesComponent},
    {path: 'referees', component: RefereesComponent},
    {path: 'games', component: GamesComponent},

    {path: 'players/:id', component: PlayerComponent},
    {path: 'players', component: PlayersComponent},

    {path: 'teams', component: TeamsComponent},
    {path: 'standings', component: StandingsComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes,{ useHash: true })],
    exports: [RouterModule]
})

export class RoutesModule {
}
