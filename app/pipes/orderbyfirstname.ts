import {Pipe, PipeTransform} from '@angular/core'

@Pipe({
    name: "orderByLastName"
})
export class OrderByLastName implements PipeTransform {

    transform(array: any[], args: string): any[] {
        console.log("Entered in pipe*******  "+ args);
        if (typeof array === "undefined") {
            return array;
        }

        array.sort(function(a, b){
            return a.player.lastname > b.player.lastname ? 1 : -1;
        })

        return array;
    }
}