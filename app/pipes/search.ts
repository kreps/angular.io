import {PipeTransform, Pipe} from "@angular/core";
@Pipe({
    name: 'searchPipe',
    pure: false
})

export class SearchPipe implements PipeTransform {
    transform(data: any[], searchTerm: string): any[] {
        searchTerm = searchTerm.toUpperCase();
        if (!data) {
            return [];
        }
        return data.filter(item => {
            return (item.player.firstname + item.player.lastname).toUpperCase().indexOf(searchTerm) !== -1
        });
    }
}