import {Pipe, PipeTransform} from '@angular/core'

@Pipe({name: 'age'})

export class AgePipe implements PipeTransform {
    transform(value: string): string {
        let years = Math.floor(((new Date()).getTime() - (new Date(value)).getTime()) / (365 * 24 * 60 * 60 * 1000))
        return  years < 100 ? years + ' a':'';
    }
}