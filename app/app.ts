///<reference path="./components/app/app.ts"/>
import './rxjs-extensions';
import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {RoutesModule} from './routes';
import {HttpModule}    from '@angular/http';
import {AppComponent} from "./components/app/app";
import {HeroDetailComponent} from "./components/hero-detail/component";
import {HeroesComponent} from "./components/heroes/component";
import {DashboardComponent} from "./components/dashboard/dashboard";
import {HeroSearchComponent} from "./components/hero-search/component";
import {LeagueComponent} from "./components/league-dropdown/component";
import {KeyUpComponent} from "./components/keyup";
import {GamesComponent} from "./components/games/component";
import {HeroFormComponent} from "./components/hero-form/component";
import {DataService} from "./services/data";
import {Logger} from "./services/logger";
import {SharedService} from "./services/shared";
import {PlacesComponent} from "./components/places/component";
import {PlayersComponent} from "./components/players/players";
import {TeamsComponent} from "./components/teams/component";
import {StandingsComponent} from "./components/standings/standings";
import {RefereesComponent} from "./components/referees/referees";
import {PlayerComponent} from "./components/player_unused/player";
import {AgePipe} from "./pipes/age";
import {NbspPipe} from "./pipes/nbsp";
import {OrderByLastName} from "./pipes/orderbyfirstname";
import {SearchPipe} from "./pipes/search";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        RoutesModule,
        HttpModule,
    ],
    declarations: [//directives, components and pipes only here
        AppComponent,
        GamesComponent,
        PlacesComponent,
        PlayersComponent,
        PlayerComponent,
        TeamsComponent,
        StandingsComponent,
        RefereesComponent,

        HeroDetailComponent,
        HeroesComponent,
        DashboardComponent,
        HeroSearchComponent,
        LeagueComponent,
        KeyUpComponent,
        HeroFormComponent,

        AgePipe,
        NbspPipe,

        OrderByLastName,
        SearchPipe

    ],
    providers: [DataService, Logger, SharedService],
    bootstrap: [AppComponent]
})

export class AppModule {
}
