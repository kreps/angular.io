import {Id} from "./id";
export interface Referee extends Id{
    firstname: string;
    lastname: string;
    level: string;
    email: string;
    region: string;
}