import {Id} from "./id";
import {League} from "./league";
export interface Place extends Id {
    name: string;
    abbr: string;
    address: string;
    link: string;
    leagues: League[];
}