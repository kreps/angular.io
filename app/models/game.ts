import {Id} from "./id";
import {Stage} from "./stage";
import {Team} from "./team";
import {GameResult} from "./gameresult";

export interface Game extends Id{
    stage: Stage;
    gameresults: GameResult[];
    round: number;
    datetime: Date;
    home: Team;
    visitor: Team;
    placeId: Object;
    result: string;
}