export interface Rival {
    team: number;
    win: boolean;
    scoreDiff: number;
}