import {Id} from "./id";
export interface League extends Id {
    name: string;
    abbreviation: string;
}