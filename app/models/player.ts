import {Id} from "./id";
export interface Player extends Id {
    firstname: string;
    lastname: string;
    height: number;
    birthdate: Date;
    age: number;
}