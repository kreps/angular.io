import {Id} from "./id";
export interface Team extends Id{
    name: string;
    abbr: string;
}