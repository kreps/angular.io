import {Engine, Engine2} from "./engine";
import {Tires} from "./Tires";

export class Car {
    engine: Engine;
    tires: Tires;
    public description = 'DI';



    constructor(engine: Engine, tires: Tires) {
    }

    drive() {
        return `${this.description} car with ` +
            `${this.engine.cylinders} cylinders and ${this.tires.make} tires.`;
    }
}

let car = new Car(new Engine2(), new Tires());