export interface Stage{
    id: number;
    name: string;
    abbr: string;
}