import {Team} from "./team";
import {Rival} from "./rival";
export interface Standing{
    team: Team;
    wins: number;
    losses: number;
    gameCount: number;
    offScore: number;
    defScore: number;
    winRatio: number;
    offAvg: number;
    defAvg: number;
    scoreDiff: number;
    rivals: Rival[];
    rivalry: number;
    rivalryDiff: number;
    points:number;
}