import {Id} from "./id";
export interface Season extends Id{
    name: string;
    isActive: boolean;
}