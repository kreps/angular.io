import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {Logger} from "./logger";
import {SharedService} from "./shared";


@Injectable()
export class DataService {

    private serverUrl: string = (window.location.protocol == "https:" ?
        "https://" + window.location.hostname + "/api/" :
        "http://" + window.location.hostname + ":1337/api/");


    constructor(private http: Http, private logger: Logger, private shared: SharedService) {
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    public get(path: string): Observable<any[]> {
        this.logger.log('Get path: '+path);
        return this.http.get(this.serverUrl + path)
            .map(this.extractData)
            .catch(this.handleError);
    }

    public getOne(path: string): Observable<any> {
        return this.http.get(this.serverUrl + path)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        console.log(error);
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            // const body = error.json() || '';
            const body = error || '';
            // const err = body.error || JSON.stringify(body);
            // errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        // console.error(errMsg);
        return Observable.throw(error);
    }


}