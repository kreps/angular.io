import {Season} from "../models/season";
import {Injectable} from "@angular/core";
import {League} from "../models/league";
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class SharedService {
    selectedSeason: Season = {id: 2017, name: '2016/2017', isActive: true};
    selectedLeague: League = {id: 6, name: 'Super Karikas', abbreviation: 'SK'};

    private leagueSource = new BehaviorSubject<League>(this.selectedLeague);
    leagueSelectItem$ = this.leagueSource.asObservable();
    changeLeague(league: League) {
        this.selectedLeague = league;
        this.leagueSource.next(league);
    }
}