import {Component, OnInit} from "@angular/core";
import {SharedService} from "../../services/shared";
import {DataService} from "../../services/data";
import {Router} from "@angular/router";
import {Observable} from "rxjs/Observable";
import {Place} from "../../models/place";
@Component({
    moduleId: module.id,
    selector: 'places-selector',
    templateUrl: 'template.html',
    styleUrls: ['style.css']
})

export class PlacesComponent implements OnInit {
    places: Place[];
    errorMessage: string;

    constructor(private router: Router, private dataService: DataService, private shared: SharedService) {
    }

    ngOnInit(): void {
        this.getPlaces();
    }

    private getPlaces() {
        this.dataService.get('places_season_league?season=2017').subscribe(
            res => this.prepareData(res),
            error => this.errorMessage = <any>error);
    }

    private prepareData(res: any) {
        // let p2: Map<number, Object> = new Map();
        this.places = [];
        for (let p of res) {
            var place = this.places.filter(el => p.place.id == el.id)[0];
            if (place){
                place.leagues.push(p.league);
            } else {
                var obj = p.place;
                obj.leagues = [];
                obj.leagues.push(p.league);
                this.places.push(obj);
            }
        }

        // for (let p of )
    }

}