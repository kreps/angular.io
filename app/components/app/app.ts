import {Component} from '@angular/core';
import {Subscription} from "../../../node_modules/rxjs/Subscription";
import {Router, NavigationEnd, NavigationStart} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: './app.html',
    styleUrls:['./app.css']
})

export class AppComponent{

    constructor(private router: Router) {
    }


}