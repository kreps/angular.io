import {Component} from "@angular/core";
import {Hero} from "../../models/hero";
@Component({
    moduleId: module.id,
    selector: 'hero-form',
    templateUrl: 'template.html',
    styleUrls:['style.css']
})

export class HeroFormComponent {
    powers = ['Really Smart', 'Super Flexible',
        'Super Hot', 'Weather Changer'];

    model = new Hero(1, 'Dr IQ', this.powers[0], 'Chuck Overstreet');
    submitted = false;

    onSubmit() {
        this.submitted = true;
    }

    get diagnostic() {
        return JSON.stringify(this.model);
    }

    newHero() {
        this.model = new Hero(42, '', '');
    }

}