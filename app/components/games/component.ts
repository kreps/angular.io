import {DataService} from "../../services/data";
import {Component, OnInit, ElementRef, ViewChild, EventEmitter, Input} from "@angular/core";
import {Game} from "../../models/game";
import {Router} from "@angular/router";
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {SharedService} from "../../services/shared";
import {Team} from "../../models/team";
import {Subscription} from "rxjs/Subscription";

@Component({
    moduleId: module.id,
    selector: 'games-selector',
    templateUrl: 'template.html',
    styleUrls: ['style.css']
})

export class GamesComponent implements OnInit {
    games: Game[];
    teamnames: Team[];
    errorMessage: string;
    subscription: Subscription;

    constructor(private router: Router, private dataService: DataService, private shared: SharedService) {
    }

    ngOnInit(): void {
        this.subscription = this.shared.leagueSelectItem$.subscribe(
            league => this.getData()
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    getData(): void {
        Observable.forkJoin([
            this.dataService.get('games?season=' + this.shared.selectedSeason.id + '&league=' + this.shared.selectedLeague.id + '&sort=datetime ASC'),
            this.dataService.get('teams_season_league?season=' + this.shared.selectedSeason.id + '&league=' + this.shared.selectedLeague.id),
        ]).subscribe(
            res => this.prepareData(res),
            error => this.errorMessage = <any>error);
    }

    private prepareData(res: Array<any>) {
        this.games = res[0];
        this.teamnames = res[1];
        this.prepareTeams();
    }

    private prepareTeams() {
        for (var game of this.games) {
            this.setHomeTeam(game);
            this.setVisitorTeam(game);
            this.setGameResult(game);
        }
    }

    private setGameResult(game: any) {
        game.result = 'vs';
        // game.datetime = new Date(game.datetime);
        if (game.gameresults[0].score + game.gameresults[1].score > 0) {
            game.result = game.gameresults[0].score + ':' + game.gameresults[1].score;
        }
    }

    private setVisitorTeam(game: any) {
        let team = (this.teamnames.filter(tn => tn.id === game.visitor.id))[0];
        if (team) {
            game.visitor = team;
        }
    }

    private setHomeTeam(game: any) {
        let team = (this.teamnames.filter(tn => tn.id === game.home.id))[0];
        if (team) {
            game.home = team;
        }
    }
}