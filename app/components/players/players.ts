import { Component, OnInit } from "@angular/core";
import { Player } from "../../models/player";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";
import { DataService } from "../../services/data";
import { SharedService } from "../../services/shared";
import { Observable } from "rxjs/Observable";
import { Input } from "../../../node_modules/@angular/core/src/metadata/directives";
@Component({
    moduleId: module.id,
    selector: 'players-selector',
    templateUrl: 'players.html',
    styleUrls: ['players.css']
})

export class PlayersComponent implements OnInit {
    players: any[];
    subscription: Subscription;
    errorMessage: string;
    selectedPlayer: any;

    selectedPlayerGames: any[];
    gamecount: number = 0;
    tpm: number = 0;//3pm
    fta: number = 0;
    ftm: number = 0;
    pf: number = 0;
    tf: number = 0;
    uf: number = 0;
    df: number = 0;
    pts: number = 0;
    start5: number = 0;
    search: string = "";


    ngOnChanges(changes: any) {
        console.log(changes);
    }

    ngOnInit(): void {
        this.subscription = this.shared.leagueSelectItem$.subscribe(
            league => {
                this.getPlayers();
                this.search = '';
            }
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    constructor(private router: Router, private dataService: DataService, private shared: SharedService) {
    }

    private getPlayers() {
        this.dataService.get('players_season_league_team?season=' + this.shared.selectedSeason.id + '&league=' + this.shared.selectedLeague.id).subscribe(
            res => this.preparePlayers(res),
            error => this.errorMessage = <any>error
        );
    }

    preparePlayers(res: any[]) {
        // for (var p of res) {
        //     p.name = p.player.firstname+p.player.lastname;
        // }
        this.players = res;
    }

    showSelectedPlayer(player: any): void {
        this.selectedPlayer = player;
        this.dataService.get('boxscores?season_id=' + this.shared.selectedSeason.id + '&league_id=' + this.shared.selectedLeague.id + '&player_id=' + player.player.id).subscribe(
            res => {
                this.selectedPlayerGames = res;
                this.preparePlayerData();
            },
            error => this.errorMessage = <any> error
        );
        // Observable.forkJoin([
        //     this.dataService.getOne('players/' + id),
        //     this.dataService.get('teams_season_league?season=' + this.shared.selectedSeason.id + '&league=' + this.shared.selectedLeague.id),
        // ]).subscribe(
        //     res => this.prepareData(res),
        //     error => this.errorMessage = <any>error);
    }

    hideSelectedPlayer(): void {
        this.selectedPlayer = null;
    }

    goToPlayerDetail(id: number) {
        this.router.navigate(['/players', id]);
    }

    private preparePlayerData() {
        this.gamecount = this.tpm = this.fta = this.ftm = this.pf = this.tf = this.uf = this.df = this.pts = this.start5 = 0;
        for (var r of this.selectedPlayerGames) {
            this.gamecount++;
            this.tpm += r.tpm;
            this.fta += r.fta;
            this.ftm += r.ftm;
            this.pf += r.pf + this.uf + this.df + this.tf;
            this.uf += r.uf;
            this.tf += r.tf;
            this.df += r.df;
            this.pts += r.pts;
            this.start5 += r.isStarter;
        }
    }
}