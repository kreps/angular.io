import {Component, OnInit} from "@angular/core";
import {DataService} from "../../services/data";
import {Referee} from "../../models/referee";
@Component({
    moduleId: module.id,
    templateUrl: 'referees.html',
    selector: 'referees-selector',
    styleUrls: ['referees.css']
})
export class RefereesComponent implements OnInit {
    referees: Referee[];
    errorMessage: string;

    constructor(private dataService: DataService) {
    }

    ngOnInit(): void {
        this.getData();
    }

    private getData() {
        this.dataService.get('referees').subscribe(
            res => this.referees = res,
            error => this.errorMessage = <any>error);

    }
}