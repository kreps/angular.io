import {Component, OnInit} from "@angular/core";
import {Team} from "../../models/team";
import {DataService} from "../../services/data";
import {Router} from "@angular/router";
import {SharedService} from "../../services/shared";
import {Subscription} from "rxjs";
@Component({
    moduleId: module.id,
    selector: 'teams-selector',
    templateUrl: 'template.html',
    styleUrls: ['style.css']
})

export class TeamsComponent implements OnInit {
    teams: any[];
    errorMessage: string;
    subscription: Subscription;

    ngOnInit(): void {
        this.subscription = this.shared.leagueSelectItem$.subscribe(
            league => this.getTeams()
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    constructor(private router: Router, private dataService: DataService, private shared: SharedService) {
    }

    private getTeams() {
        this.dataService.get('teams_season_league?season=' + this.shared.selectedSeason.id + '&league=' + this.shared.selectedLeague.id + '&sort=name%20ASC').subscribe(
            res => this.teams = res,
            error => this.errorMessage = <any>error
        );
    }
}