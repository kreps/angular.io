import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';
import 'rxjs/add/operator/switchMap';
import {DataService} from "../../services/data";
import {Player} from "../../models/player";
import {SharedService} from "../../services/shared";

@Component({
    moduleId: module.id,
    selector: 'player-selector',
    templateUrl: 'player.html',
    styleUrls: ['player.css'],
})

export class PlayerComponent implements OnInit {
    @Input()
    player: Player;
    errorMessage: string;

    constructor(private dataService: DataService, private route: ActivatedRoute, private location: Location, private shared: SharedService) {
    }

    ngOnInit(): void {
        let id = this.route.snapshot.params['id'];
        this.dataService.getOne('players/' + id).subscribe(
            res => this.player = res,
            error => this.errorMessage = <any>error);
    }

    goBack(): void {
        this.location.back();
    }
}
