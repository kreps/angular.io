import {Component, OnInit} from '@angular/core';
import {Hero} from '../../models/hero';
import {Router} from "@angular/router";
import {DataService} from "../../services/data";

@Component({
    moduleId: module.id,
    selector: 'my-heroes',
    templateUrl: 'template.html',
    styleUrls: ['style.css']
})


export class HeroesComponent implements OnInit {
    heroes: Hero[];
    selectedHero: Hero;
    errorMessage: string;
    name = 'Angular';

    constructor(private router: Router,
                private dataService: DataService) {
    }

    ngOnInit(): void {
        this.getHeroes();
    }

    gotoDetail(): void {
        this.router.navigate(['/heroes', this.selectedHero.id]);
    }

    getHeroes(): void {
        this.dataService.get('heroes').subscribe(
            res => this.heroes = res,
            error => this.errorMessage = <any>error);
        // this.heroService.getHeroesSlowly().then(heroes => this.heroes = heroes);
    }

    onSelect(hero: Hero): void {
        this.selectedHero = hero;
    }

    add(name: string): void {
        name = name.trim();
        if (!name) {
            return;
        }
        // this.heroService.create(name)
        //     .then(hero => {
        //         this.heroes.push(hero);
        //         this.selectedHero = null;
        //     });
    }

    delete(hero: Hero): void {
        // this.heroService
        //     .delete(hero.id)
        //     .then(() => {
        //         console.log(hero);
        //         this.heroes = this.heroes.filter(h => h.id !== hero.id);
        //         if (this.selectedHero === hero) {
        //             this.selectedHero = null;
        //         }
        //     });


    }
}
