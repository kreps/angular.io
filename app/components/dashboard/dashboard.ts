import {Component, OnInit} from '@angular/core';
import {Hero} from '../../models/hero';
import {DataService} from "../../services/data";

@Component({
    moduleId: module.id,
    selector: 'my-dashboard',
    templateUrl: 'dashboard.html',
    styleUrls: ['dashboard.css']
})

export class DashboardComponent implements OnInit {
    heroes: Hero[];
    errorMessage: string;

    constructor(private dataService: DataService) {
    }

    ngOnInit(): void {
        this.dataService.get('heroes')
            .subscribe(
                res => this.heroes = res,
                error => this.errorMessage = <any>error);
    }
}
