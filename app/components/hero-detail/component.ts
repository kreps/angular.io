import {Component, Input, OnInit} from '@angular/core';
import {Hero} from '../../models/hero';
import {ActivatedRoute, Params} from '@angular/router';
import {Location} from '@angular/common';
import 'rxjs/add/operator/switchMap';
import {DataService} from "../../services/data";
import {Observable} from "rxjs/Observable";

@Component({
    moduleId: module.id,
    selector: 'my-hero-detail',
    templateUrl: 'template.html',
    styleUrls: ['style.css'],
})

export class HeroDetailComponent implements OnInit {
    @Input()
    hero: Hero;
    errorMessage: string;

    constructor(private dataService: DataService,
                private route: ActivatedRoute,
                private location: Location) {
    }

    ngOnInit(): void {
        let id = this.route.snapshot.params['id'];
        this.dataService.getOne('heroes/' + id).subscribe(
            res => this.hero = res,
            error => this.errorMessage = <any>error);
    }

    // save(): void {
    //     this.heroService.update(this.hero).then(() => this.goBack());
    // }

    goBack(): void {
        this.location.back();
    }
}
