import {Component, OnInit} from "@angular/core";
import {Subscription} from "rxjs/Subscription";
import {DataService} from "../../services/data";
import {SharedService} from "../../services/shared";
import {Game} from "../../models/game";
import {Team} from "../../models/team";
import {Observable} from "rxjs/Observable";
import {Standing} from "../../models/standing";
import {Rival} from "../../models/rival";
import {Logger} from "../../services/logger";

@Component({
    moduleId: module.id,
    selector: 'standings-selector',
    templateUrl: './standings.html',
    styleUrls: ['./standings.css']
})

export class StandingsComponent implements OnInit {
    games: Game[];
    teamnames: Team[];
    standings: Standing[] = [];
    errorMessage: string;
    subscription: Subscription;

    constructor(private dataService: DataService, private shared: SharedService, private logger: Logger) {
    }

    ngOnInit(): void {
        this.subscription = this.shared.leagueSelectItem$.subscribe(
            league => this.getGames()
        );
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    getGames(): void {
        Observable.forkJoin([
            this.dataService.get('games?season=' + this.shared.selectedSeason.id + '&league=' + this.shared.selectedLeague.id + '&sort=datetime ASC'),
            this.dataService.get('teams_season_league?season=' + this.shared.selectedSeason.id + '&league=' + this.shared.selectedLeague.id),
        ]).subscribe(
            res => this.prepareData(res),
            error => this.errorMessage = <any>error);
    }

    private prepareData(res: Array<any>) {
        this.games = res[0];
        this.games = this.games.filter(el => el.stage.id == 1);
        this.teamnames = res[1];
        this.setStandings();
        this.sortStandings();
    }


    private setStandings() {
        this.standings = [];
        for (let game of this.games) {
            let res1 = game.gameresults[0];
            let res2 = game.gameresults[1];
            if (res1.score != res2.score) {
                this.setGameResult(res1, res2);
                this.setGameResult(res2, res1);
            }
        }
        this.setAveragesAndRatios();
        this.setRivalty();
        this.setRivaltyDiff();
    }

    private setGameResult(res1: any, res2: any) {
        let obj = this.getStandingByTeamId(res1.team);
        if (obj) {
            StandingsComponent.updateTeam(obj, res1, res2);
        } else {
            this.addTeam(res1, res2);
        }
    }

    private getStandingByTeamId(teamId: number): Standing {
        return this.standings.filter(el => teamId == el.team.id)[0];
    }

    private getTeamById(teamId: number): Team {
        return this.teamnames.filter(el => teamId == el.id)[0];
    }

    private static updateTeam(standing: Standing, result: any, opponentResult: any) {
        standing.wins += (result.score > opponentResult.score ? 1 : 0);
        standing.losses += (result.score < opponentResult.score ? 1 : 0);
        standing.gameCount += (result.score != opponentResult.score ? 1 : 0);
        standing.offScore += result.score;
        standing.defScore += opponentResult.score;
        standing.rivals.push({
            team: opponentResult.team,
            win: result.score > opponentResult.score,
            scoreDiff: result.score - opponentResult.score
        });
        standing.points += (result.score > opponentResult.score ? 2 : (result.score == 0 && opponentResult.score == 20) ? 0 : 1);
    }

    private addTeam(result: any, opponentResult: any) {
        let team = this.getTeamById(result.team);
        if (team) {//todo superkarika võistkonnad > remove this code
            let s = this.createStanding(team, result, opponentResult);
            this.standings.push(s);
        }
    }

    private createStanding(team: Team, result: any, opponentResult: any) {
        let s = <Standing> {
            team: team,
            wins: (result.score > opponentResult.score ? 1 : 0),
            losses: (result.score < opponentResult.score ? 1 : 0),
            gameCount: (result.score != opponentResult.score ? 1 : 0),
            offScore: <number> result.score,
            defScore: <number> opponentResult.score,
            winRatio: 0,
            offAvg: 0,
            defAvg: 0,
            scoreDiff: 0,
            rivals: [{
                team: opponentResult.team,
                win: result.score > opponentResult.score,
                scoreDiff: result.score - opponentResult.score
            }],
            rivalry: 0,
            rivalryDiff: 0,
            points: (result.score > opponentResult.score ? 2 : (result.score == 0 && opponentResult.score == 20) ? 0 : 1)
        };
        return s;
    }

    private sortStandings() {
        this.standings = this.standings.sort(function (a, b) {
            if (a.points != b.points) {
                return b.points - a.points;
            }
            if (a.points == b.points && b.rivalry != a.rivalry) {
                return b.rivalry - a.rivalry;
            }

            if (b.rivalry == a.rivalry && b.rivalryDiff != a.rivalryDiff) {
                return b.rivalryDiff - a.rivalryDiff;
            }
            //total point diff
            return b.scoreDiff - a.scoreDiff;
        });
    }

    private setAveragesAndRatios() {
        for (let s of this.standings) {
            s.winRatio = s.wins / s.gameCount;
            s.offAvg = s.offScore / s.gameCount;
            s.defAvg = s.defScore / s.gameCount;
            s.scoreDiff = s.offScore - s.defScore;
        }
    }

    private setRivalty() {
        for (var s1 of this.standings) {
            for (let s2 of this.standings) {
                if (s1.team.id != s2.team.id && s1.points == s2.points) {
                    let rival = s1.rivals.filter(el => s2.team.id == el.team)[0];
                    if (rival) {
                        s1.rivalry += (rival.win == true ? 1 : 0);
                    }
                }
            }
        }
    }

    private setRivaltyDiff() {
        for (var s1 of this.standings) {
            for (let s2 of this.standings) {
                if (s1.team.id != s2.team.id && s1.points == s2.points && s1.rivalry == s2.rivalry) {
                    let rival = s1.rivals.filter(el => s2.team.id == el.team)[0];
                    if (rival) {
                        s1.rivalryDiff += rival.scoreDiff;
                    }
                }
            }
        }
    }

}
