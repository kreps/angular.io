import {OnInit, Component} from "@angular/core";
import {HeroSearchService} from "../../services/hero-search.service";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";
import {Hero} from "../../models/hero";
import {Router} from "@angular/router";

@Component({
        moduleId: module.id,
        selector: 'hero-search',
        templateUrl: 'template.html',
        styleUrls: ['style.css'],
        providers: [HeroSearchService]
    }
)

export class HeroSearchComponent implements OnInit {

    heroes: Observable<Hero[]>;
    private searchTerms = new Subject<string>();

    constructor(private heroSearchService: HeroSearchService,
                private router: Router) {
    }

    search(term: string): void {
        this.searchTerms.next(term);
    }

    ngOnInit(): void {
        this.heroes = this.searchTerms
            .debounceTime(300)        // wait for 300ms pause in events
            .distinctUntilChanged()   // ignore if next search term is same as previous
            .switchMap(term => term   // switch to new observable each time
                // return the http search observable
                ? this.heroSearchService.search(term)
                // or the observable of empty heroes if no search term
                : Observable.of<Hero[]>([]))
            .catch(error => {
                console.log(error);
                return Observable.of<Hero[]>([]);
            });
    }

    gotoDetail(hero: Hero): void {
        let link = ['heroes', hero.id];
        this.router.navigate(link);
    }
}