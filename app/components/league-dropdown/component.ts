import {Component, OnInit, ElementRef, Output, EventEmitter} from "@angular/core";
import {DataService} from "../../services/data";
import {League} from "../../models/league";
import {SharedService} from "../../services/shared";
import {Router} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'league-select-component-selector',
    providers: [DataService],
    templateUrl: 'template.html',
    styleUrls: ['style.css'],
    host: {
        '(document:click)': 'onClick($event)',
    },
})

export class LeagueComponent implements OnInit {
    errorMessage: string;
    leagues: League[];
    menuIsShown: boolean;
    selectedLeague: any;

    constructor(private router: Router, private dataService: DataService, private element: ElementRef, private shared: SharedService) {
        this.selectedLeague = this.shared.selectedLeague;
    }

    ngOnInit(): void {
        this.menuIsShown = false;
        this.getAllItems();
    }

    getAllItems() {
        this.dataService.get('leagues?sort=sort asc')
            .subscribe(
                res => {
                    this.leagues = res;
                    this.shared.selectedLeague = res[0];
                },
                error => this.errorMessage = <any>error);
    }

    toggle() {
        this.menuIsShown = !this.menuIsShown;
    }

    subItemClick(league: League) {
        this.selectedLeague = league;
        this.shared.changeLeague(league);
        this.menuIsShown = false;
    }

    onClick() {
        if (!this.element.nativeElement.contains(event.target)) {
            this.menuIsShown = false;
        }
    }
}