import {Component} from "@angular/core";
@Component({
    selector: 'key-up',
    template: `
<input #box (keyup)="onKey(box.value)" (keyup.enter)="onEnter(box.value); box.value = '';" (blur)="onBlur(box.value)">
    <p>{{info}}</p>
    <div *ngFor="let t of values">{{t}}></div><br>
`
})
export class KeyUpComponent {
    info = '';
    values = ['test'];

    onKey(value: string) { // without type info
        this.info = '...typing';
    }

    onEnter(value: string){
        this.values.push(value);

    }

    onBlur(value: string){
        this.info = 'Blurred';
    }
}