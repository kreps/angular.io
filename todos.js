/*
 NEXT
 done > schedule date + time > datetime
 done > create teamnames_season_league
 done > track league change to selected component
 todo > Ava leht, latest season
 done > leagues select component
 todo > AOT compilation with gulp
 done > teams_season_league refactor
 done > add name from teams table
 done > teams_season_league > id:season:league:base_team_id:name:abbr
 done > teams (default name and abbr) > id:name:abbr
 */

/*
 Kasutaja vaade
 done > andmebaasi migratsiooni skript: see võimaldab kiiret üleminekut uuele lehele suvalisel aja hetkel, ka kui uue hooaja andmed juba on sisestatud andmebaasi korrastus, tähe vead, topelt meekonnad, mitte kehtivad mängud jms.
 done > liiga valik
 done > db > lisada super karikas
 done > db > saalid universal update
 done > võistluskohtade üldvaade
 done > võistluskohtade liigad data
 done > võistluskohtade kaardi pop out
 done > mängijate üldvaade
 done > mängija vanus a2
 done > mängijate nimekirja sorteerimine perenime järgi
 todo > superkarika mängijad
 todo > superkarika võistkonnad
 done > mängijate otsing
 todo > mängija tiimi nimi vastavalt hooajale
 todo > ajakava/tulemused otsing
 todo > team stats > avg team score > best def > best off team >
 todo > games > show team name (W-L) > home -vistor as badge or table container
 done > võistkondade üldvaade
 done > ajakava/tulemused üldvaade
 done > games > nähtavad varasemad ülemineku mängud
 done > gameresults > DB refactor > season 2017:team
 done > turniiritable
 done > turniiritable surnud ringi omavahelised mängud
 done > turniiritable surnud ringi omavaheliste mängude puntkivahe
 done > turniiritable turna punktide arvestus 2- võ, 1-ka, 0- loobumine
 done > info leht/menüü > kohtunike vaade
 done > mängija detailne vaade
 done > mängija + statistika (mängude sum, osalus %, võidu %, , 3pt avg,  ft%, ftavg, pts avg
 todo > mängija osalus %, võidu %
 todo > mängija ftsum, 3pt sum, pts sum,

 --DEV--
 todo > player previous teams + avg stats,
 todo > võistkonna detailne vaade
 todo > mängu tulemuse detailne vaade (boxscore)
 todo > ajakava/tulemused playoff vaade (nö klambri vaade) uus
 todo > info lehed: kontakt, juhend, eemaldamised, uudised
 todo > hooaja valik
 todo > lehe disaini kinnitmaine

 LATER
 todo > teams_season_league drop > team_id_drop, _leaguename_drop
 */