var gulp = require('gulp');
// var pug = require('gulp-pug');
var less = require('gulp-less');
// var minifyCSS = require('gulp-csso');

// gulp.task('html', function(){
//     return gulp.src('client/templates/*.pug')
//         .pipe(pug())
//         .pipe(gulp.dest('build/html'))
// });

gulp.task('less', function(){
    return gulp.src(['*.less', 'app/**/*.less', 'assets/*.less'],{base: './'})
        .pipe(less())
        .pipe(gulp.dest('./'))
});

// gulp.task('default', [ 'less' ]);